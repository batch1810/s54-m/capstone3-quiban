import { Link } from 'react-router-dom';
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import NavDropdown from 'react-bootstrap/NavDropdown';
import * as Icon from 'react-feather';

export default function AdminNavs() {
  return (
    <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" fixed="top" className="border-bottom">
      <Container>
        <Navbar.Brand as={Link} to="/">Admin</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link as={Link} to="/"><Icon.Monitor size={20}/> <span className="align-middle">Dashboard</span></Nav.Link>
            <Nav.Link as={Link} to="/products"><Icon.Grid size={20}/> <span className="align-middle">Products</span></Nav.Link>
          </Nav>
          <Nav>
          <NavDropdown title="Admin" id="profile-nav-dropdown">
                <NavDropdown.Item href="#">Change Password</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item as={Link} to="/logout">
                    Logout
                </NavDropdown.Item>
                </NavDropdown>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}