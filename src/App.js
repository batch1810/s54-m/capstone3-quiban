import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import AdminNavbar from "./AdminNavbar";
import AdminDashboard from "../pages/AdminDashboard";
import AdminAllProducts from "../pages/AdminAllProducts";
import AdminAddProduct from "../pages/AdminAddProduct";
import Login from "../pages/Login";
import Logout from "../pages/Logout";
import Container from "react-bootstrap/Container";
import './App.css';

function App() {
  return (
    <>
      <Router>
            <AdminNavbar/>
            <Container className="mt-5 pt-4">
                <Routes>
                    <Route exact path="/" element={<AdminDashboard />} />
                    <Route exact path="/products" element={<AdminAllProducts />} />
                    <Route exact path="/products/add" element={<AdminAddProduct />} />
                    <Route exact path="/login" element={<Login />} />
                    <Route exact path="/logout" element={<Logout />} />
                </Routes>
            </Container>
        </Router>
    </>
  );
}

export default App;
