import { useEffect, useState, useContext } from "react";
import { Link, useParams, useNavigate } from "react-router-dom";
import UserContext from "../UserContext";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

export default function ProductView() {
    const { user, cartItemCount, setCartItemCount } = useContext(UserContext);

    const { productSlug } = useParams();
    const navigate = useNavigate();

    // Active product
    const [category, setCategory] = useState(null);
    const [product, setProduct] = useState(null);

    async function getProduct() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/products/${productSlug}/product`);
        const data = await res.json();
        if (data.category.length > 0) {
            setCategory(data.category[0]);
        }
        if (data.product.length > 0) {
            setProduct(data.product[0]);
        }
    }

    useEffect(() => {
        getProduct();
    }, [productSlug]);

    const [quantity, setQuantity] = useState(1);

    useEffect(() => {
        if (isNaN(quantity) || (isNaN(quantity) === false && quantity <= 0)) {
            setQuantity(1);
        }
        else if (product !== null && quantity > product.stocks) {
            setQuantity(product.stocks);
        }
    }, [quantity]);

    const [alertMsg, setAlerMsg] = useState({
        variant: "light",
        title: "",
        body: ""
    });

    async function addToCart() {
        if (user.id !== null) {
            const res = await fetch(`${process.env.REACT_APP_API_URL}/orders/addToCart`, {
                method: "POST",
                headers: {
                  Authorization: `Bearer ${localStorage.getItem("token")}`,
                  "Content-Type": "application/json"
                },
                body: JSON.stringify({ productId: product._id, quantity: parseInt(quantity) })
              });
            const result = await res.json();
            if (typeof result.errorCode !== "undefined") {
                if (result.errorCode === 1) {
                    setAlerMsg({
                        variant: "warning",
                        title: "Warning!",
                        body: "You've reached the maximum quantity available for this item."
                    });
                }
                else {
                    setAlerMsg({
                        variant: "danger",
                        title: "Error!",
                        body: "Something went wrong! Please try again later."
                    });
                }
            }
            else {
                setCartItemCount(cartItemCount + parseInt(quantity));
                setAlerMsg({
                    variant: "success",
                    title: "Added to Cart!",
                    body: `${product.name} has been added to your cart successfully!`
                });
            }
            return;
        }
    }

    return (
        <>
            <div className="mt-3 mb-4 min-height-70vh">
            {
                product !== null ?
                    <>
                        <nav aria-label="breadcrumb" className="mb-3">
                            <ol className="breadcrumb">
                                <li className="breadcrumb-item"><Link to="/" className="text-decoration-none">Home</Link></li>
                                <li className="breadcrumb-item">
                                    {
                                        category === null ?
                                            <Link to="/all-products" className="text-decoration-none">All Products</Link> :
                                            <Link to={`/products/${category.slug}`} className="text-decoration-none">{category.name}</Link>
                                    }
                                </li>
                                <li className="breadcrumb-item active" aria-current="page">{product.name}</li>
                            </ol>
                        </nav>
                        <Row>
                            <Col md={6} lg={5}>
                                <img className="img-fluid mb-3" src={ product.photoUrl.trim() === "" ? "/no-product-image.jpg" : product.photoUrl} alt={product.name} />
                            </Col>
                            <Col md={6} lg={7}>
                                <h3>{product.name}</h3>
                                <p>{product.description}</p>
                                <div className="text-primary fs-4 fw-bold">{product.discount > 0 ? numberFormat.format(product.discountedPrice) : numberFormat.format(product.price)}</div>
                                {
                                    product.discount > 0 ?
                                    <div className="text-dark"><span className="text-muted text-decoration-line-through">{numberFormat.format(product.price)}</span> - {product.discount}%</div> : ""
                                }
                                <Row className="mt-3">
                                    <Col sm={6} xl={3}>
                                        <span className="text-muted">Stock: {product.stocks}</span>
                                        {
                                            product !== null && product.stocks > 0 ?
                                                <>
                                                    <Form.Group className="mb-3" controlId="quantity">
                                                        <Form.Label>Quantity</Form.Label>
                                                        <Form.Control type="number" className="rounded-0 text-center" value={quantity} onChange={(e) => setQuantity(e.target.value)} />
                                                    </Form.Group>
                                                    <div className="d-grid">
                                                        <Button variant="primary" size="lg" onClick={addToCart} className="rounded-0 text-uppercase">Add to Cart</Button>
                                                    </div> 
                                                </>
                                                : ""
                                        }
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </>
                    : ""
            }
            </div>
        </>
    )
}