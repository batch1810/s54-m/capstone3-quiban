import Button from "react-bootstrap/esm/Button";
import Card from "react-bootstrap/esm/Card";
import Row from "react-bootstrap/esm/Row";
import Col from "react-bootstrap/esm/Col";
import Form from "react-bootstrap/esm/Form";

export default function AddProduct() {
    return (
        <>
            <h2>Add Product</h2>
            <Card className="shadow-sm rounded-0">
                <Card.Body>
                    <Form noValidate method="POST">
                        <Row>
                            <Col lg={4} xl={3}>
                                <Form.Label>Product Photos</Form.Label>
                            </Col>
                            <Col lg={8} xl={9}>
                                <Row>
                                    <Form.Group as={Col} className="mb-3" controlId="product_name">
                                        <Form.Label>Product Name</Form.Label>
                                        <Form.Control type="text" className="rounded-0" />
                                    </Form.Group>
                                    <Form.Group as={Col} className="mb-3" controlId="category">
                                        <Form.Label>Category</Form.Label>
                                        <Form.Select className="rounded-0">
                                            <option>Select Category</option>
                                        </Form.Select>
                                    </Form.Group>
                                </Row>
                                <Form.Group className="mb-3" controlId="slug">
                                    <Form.Label>Slug</Form.Label>
                                    <Form.Control type="text" className="rounded-0" />
                                </Form.Group>
                                <Form.Group className="mb-3" controlId="description">
                                    <Form.Label>Description</Form.Label>
                                    <Form.Control as="textarea" rows={4} className="rounded-0" />
                                </Form.Group>
                                <Row>
                                    <Form.Group as={Col} className="mb-3" controlId="price">
                                        <Form.Label>Price</Form.Label>
                                        <Form.Control type="number" className="rounded-0" />
                                    </Form.Group>
                                    <Form.Group as={Col} className="mb-3" controlId="stock">
                                        <Form.Label>Stock</Form.Label>
                                        <Form.Control type="number" className="rounded-0" />
                                    </Form.Group>
                                </Row>
                                <Button variant="primary" type="submit" className="rounded-0" size="lg">Submit</Button>
                            </Col>
                        </Row>
                    </Form>
                </Card.Body>
            </Card>
        </>
    )
}