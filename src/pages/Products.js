import { useState, useEffect } from "react";

export default function Products() {
    const [products, setProducts] = useState([]);

    async function getProducts() {
        const res = await fetch(`${process.env.REACT_APP_API_URL}/products`);
        const data = await res.json();
        if (typeof data.category !== "undefined") {
            setProducts(data.products);
        }
    }

    useEffect(()=>{
        getProducts()
    },[])

    return (
        <>
            <div className="min-height-70vh">
                <Row>
                    {
                        products.length > 0 ? 
                            products.map(p => 
                                <Col key={p._id} sm={6} md={4} lg={3}>
                                    <Link to={`/product/${p.slug}`} className="d-block shadow-sm mb-3 text-decoration-none">                                    
                                        <Card className="rounded-0">
                                            <Card.Img variant="top" src="/no-product-image.jpg" className="rounded-0" />
                                            <Card.Body>
                                                <Card.Title className="text-nowrap text-dark text-ellipsis overflow-hidden fs-6">{p.name}</Card.Title>
                                                <div className="fs-5 fw-bold">{p.discount > 0 ? numberFormat.format(p.discountedPrice) : numberFormat.format(p.price)}</div>
                                                {
                                                    p.discount > 0 ?
                                                    <small className="text-dark"><span className="text-muted text-decoration-line-through">{numberFormat.format(p.price)}</span> - {p.discount}%</small> : <small>&nbsp;</small>
                                                }
                                            </Card.Body>
                                        </Card>
                                    </Link>
                                </Col>
                            )
                            : ""
                    }
                </Row>
            </div>
        </>
    )
}