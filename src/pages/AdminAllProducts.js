import Button from "react-bootstrap/esm/Button";
import Card from "react-bootstrap/esm/Card";
import Table from "react-bootstrap/esm/Table";
import Form from "react-bootstrap/esm/Form";
import * as Icon from "react-feather";

export default function Products() {
    return (
        <>
            <h2>All Products</h2>
            <Card className="shadow-sm rounded-0">
                <Card.Body>
                    <Table hover responsive className="table-borderless">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Name</th>
                                <th>Price</th>
                                <th>Offer</th>
                                <th>Purchased</th>
                                <th>Stock</th>
                                <th>Active</th>
                                <th>Date Added</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td className="align-middle border-top">
                                    <div className="border" style={{ width: "50px", height: "50px" }}></div>
                                </td>
                                <td className="align-middle border-top">Full Sleeve</td>
                                <td className="align-middle border-top">Php 150</td>
                                <td className="align-middle border-top">10% OFF</td>
                                <td className="align-middle border-top">2161</td>
                                <td className="align-middle border-top">200</td>
                                <td className="align-middle border-top">
                                    <Form.Check 
                                        type="switch"
                                        id="custom-switch-1"
                                        label=""
                                    />
                                </td>
                                <td className="align-middle border-top">07/25/2022</td>
                                <td className="align-middle border-top">
                                    <Button variant="link" size="sm">
                                        <Icon.Edit size={18}/>
                                    </Button>
                                    <Button variant="link" size="sm">
                                        <Icon.Trash size={18}/>
                                    </Button>
                                </td>
                            </tr>
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        </>
    )
}