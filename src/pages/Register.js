import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import validateEmail from '../utils/validateEmail';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';

export default function Register() {
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [email, setEmail] = useState("");
    const [emailValidated, setEmailValidated] = useState(null);
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [pwdMatched, setPwdMatched] = useState(null);

    const [btnEnabled, setBtnEnabled] = useState(false);

    const [emailExist, setEmailExist] = useState(null);
    const [success, setSuccess] = useState(null);

    const register = async (e) => {
        e.preventDefault();

        if (btnEnabled === false) return;

        setEmailExist(null);
        setSuccess(null);

        const checkEmailRes = await fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
            method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({ email })
        });
        const exist = await checkEmailRes.json();
        setEmailExist(exist);
        if (exist === false) {
            const res = await fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ firstName, lastName, email, password })
            });
            const resSuccess = await res.json();
            setSuccess(resSuccess);
            if (resSuccess) {
                setFirstName("");
                setLastName("");
                setEmail("");
                setPassword("");
                setConfirmPassword("");
            }
        }
    };

    useEffect(()=>{
        let isValid = true;

        if (firstName === "" || lastName === "" || email === "" || password === "" || confirmPassword === "") {
            isValid = false;
        }

        if (email !== "" && !validateEmail(email)) { 
            isValid = false; 
            setEmailValidated(false);
        }
        else {
            setEmailValidated(null);
        }

        if (password !== "" && confirmPassword !== "" && password !== confirmPassword) {
            isValid = false; 
            setPwdMatched(false);
        }
        else {
            setPwdMatched(null);
        }
        
        if (isValid) {
            setBtnEnabled(true);
        }
        else {
            setBtnEnabled(false);
        }
    }, [firstName, lastName, email, password, confirmPassword]);

    return (
        <Row>
            <Col md={{span: 8, offset: 2}} lg={{span: 4, offset: 4}}>
                <h2 className="text-center mb-3 mt-5">Create Account</h2>
                {
                    emailExist === true || success === false ? 
                    <Alert key="danger" variant="danger" >
                        {emailExist === true ? "Sorry your email is already registered!" : "Something went wrong! Please try again later."}
                    </Alert> : ""
                }
                {
                    success ?
                    <Alert key="success" variant="success">
                        <strong>Success!</strong> Your account has been created. You can now <Link to="/login" className="text-decoration-none">login</Link>.
                    </Alert> : ""
                }
                <Form noValidate method="POST" onSubmit={register}>
                    <Row>
                        <Form.Group as={Col} className="mb-2" controlId="firstname">
                            <Form.Label>First Name <span className="text-danger">*</span></Form.Label>
                            <Form.Control type="text" value={firstName} onChange={e => setFirstName(e.target.value)} required />
                        </Form.Group>
                        <Form.Group as={Col} className="mb-2" controlId="lastname">
                            <Form.Label>Last Name <span className="text-danger">*</span></Form.Label>
                            <Form.Control type="text" value={lastName} onChange={e => setLastName(e.target.value)} required />
                        </Form.Group>
                    </Row>
                    <Form.Group className="mb-2" controlId="email">
                        <Form.Label>Email <span className="text-danger">*</span></Form.Label>
                        <Form.Control type="email" className={emailValidated === false ? "border-danger" : ""} value={email} onChange={e => setEmail(e.target.value)} required />
                        <span className="text-danger">{ emailValidated === false ? "Email is invalid." : "" }</span>
                    </Form.Group>
                    <Form.Group className="mb-2" controlId="password">
                        <Form.Label>Password <span className="text-danger">*</span></Form.Label>
                        <Form.Control type="password" aria-describedby="passwordHelpBlock"  value={password} onChange={e => setPassword(e.target.value)} required/>                        
                    </Form.Group>
                    <Form.Group className="mb-3" controlId="confirmPassword">
                        <Form.Label>Confirm Password <span className="text-danger">*</span></Form.Label>
                        <Form.Control type="password" className={pwdMatched === false ? "border-danger" : ""} value={confirmPassword} onChange={e => setConfirmPassword(e.target.value)} required/>
                        <span className="text-danger">{ pwdMatched === false ? "Your password and confirmation password does not match" : "" }</span>
                    </Form.Group>
                    <div className="d-grid">
                        {
                            btnEnabled ?
                            <Button variant="primary" type="submit">
                                Register
                            </Button> :
                            <Button variant="primary" type="submit" disabled>
                                Register
                            </Button>
                        }
                    </div>
                </Form>
            </Col>
        </Row>
    )
}